# set container OS to OpenShift's CentOS 7
FROM openshift/base-centos7

MAINTAINER Chris Watson (cwatson@cern.ch) c/o SUPERVISOR Roland Sipos (rsipos@cern.ch)

USER 1001

LABEL io.openshift.expose-services="8080:http"

# Defines the location of the S2I
# Although this is defined in openshift/base-centos7 image it's repeated here
# to make it clear why the following COPY operation is happening
LABEL io.openshift.s2i.scripts-url=image:///usr/local/s2i

COPY ./s2i/bin/ /usr/libexec/s2i

# update yum definitions and upgrade any packages that need upgraded
RUN yum update -y && yum upgrade -y
# install basic tools for downloading, extracting & managing files
RUN yum install wget git gcc openssl-devel bzip2-devel -y
# install c++ package for gcc, tkinter and sqlite packages for later w/ python3.6.5 and JVM/JDK
RUN yum install gcc-c++ make tk-devel sqlite-devel java-1.8.0-openjdk-devel -y
# check for any upgrades
RUN yum upgrade -y && yum clean all -y
# download & extract Python3.6.5 files, since CentOS only ships with 2.7.5
RUN cd /usr/src && wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tar.xz && tar xvf Python-3.6.5.tar.xz
# configure python2.7.15 for sqlite & tkinter pkgs & run alternate install script
RUN cd /usr/src/Python-3.6.5 && ./configure --enable-optimizations && make altinstall
# validate correct version install
RUN python3.6 -V
# upgrade to latest Pip for Python3.6.5
RUN pip3.6 install --upgrade pip
# validate correct version install
RUN pip3.6 -V
# install packages for pytimber application
RUN pip3.6 install six matplotlib bottle IPython pytimber scipy django
# install fuzzy finder for command line use (DEVELOPER-HELP)
RUN git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && ~/.fzf/install --all

# Drop the root user and make the content of /opt/app-root owned by user 1001
RUN chown -R 1001:1001 /opt/app-root

# Specify the ports the final image will expose
EXPOSE 8080

# Set the default CMD to print the usage of the image, if somebody does docker run
CMD ["usage"]
