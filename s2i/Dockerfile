# set container OS to OpenShift's CentOS 7
FROM openshift/base-centos7

MAINTAINER Chris Watson (cwatson@cern.ch) c/o SUPERVISOR Roland Sipos (rsipos@cern.ch)

LABEL io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,html,django,python36" \
      io.k8s.description="Platform for serving pytimber web-app over Django"

ARG GIT_USER
ARG GIT_TOKEN

ENV DIMDIR=/usr/src/dim
ENV LD_LIBRARY_PATH=/usr/src/dim/linux
ENV DIPNS=dipnsgpn1,dipnsgpn2
ENV DIM_DNS_PORT=2506

# update yum definitions and upgrade any packages that need upgraded
RUN yum update -y && yum upgrade -y && yum -y clean all
# install basic tools for downloading, extracting & managing files
RUN yum install wget git gcc openssl-devel bzip2-devel unzip -y && yum -y clean all
# install c++ package for gcc, tkinter and sqlite packages for later w/ python3.6.5 and JVM/JDK
RUN yum install gcc-c++ make tk-devel sqlite-devel java-1.8.0-openjdk-devel epel-release -y && yum -y clean all
# install python36 from yum
RUN yum --enablerepo epel install python36 python36-devel python36-libs python36-tkinter python36-tools -y && yum -y clean all
# check for any upgrades
RUN yum upgrade -y && yum clean all -y
# delete any missed 'orphaned' files/packages
RUN rm -rf /var/cache/yum
# start configuing DIM
RUN cd /usr/src && wget http://dim.web.cern.ch/dim/dim_v20r23.zip && unzip -a dim_v20r23.zip && mv dim_v20r23 dim && rm dim_v20r23.zip
# collect latest pip and install
RUN cd /tmp && wget https://bootstrap.pypa.io/get-pip.py && python36 get-pip.py && rm get-pip.py
# install packages for pytimber application
RUN pip install six matplotlib bottle IPython pytimber scipy django numpy
# install fuzzy finder for command line use (DEVELOPER-HELP)
RUN git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && ~/.fzf/install --all
# install pyDIM
RUN cd /usr/src && git clone -b feature/upgrade_python2_to_python3 --single-branch https://$GIT_USER:$GIT_TOKEN@gitlab.cern.ch/lhcb-online/pydim.git && cd pydim && python36 setup.py install

# Defines the location of the S2I
# Although this is defined in openshift/base-centos7 image it's repeated here
# to make it clear why the following COPY operation is happening
LABEL io.openshift.s2i.scripts-url=image:///usr/local/s2i

# Copy the S2I scripts from ./.s2i/bin/ to /usr/local/s2i when making the builder image
COPY ./s2i/bin/ /usr/local/s2i

# Drop the root user and make the content of /opt/app-root owned by user 1001
RUN chown -R 1001:1001 /opt/app-root

ENV DIM_DNS_NODE=cs-ccr-dip1.cern.ch,cs-ccr-dip2.cern.ch

# Set the default user for the image, the user itself was created in the base image
USER 1001

# Specify the ports the final image will expose
EXPOSE 8080

# Set the default CMD to print the usage of the image, if somebody does docker run
CMD ["usage"]
