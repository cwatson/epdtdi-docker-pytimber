backcall==0.1.0
bottle==0.12.13
certifi==2018.4.16
chardet==3.0.4
cmmnbuild-dep-manager==2.1.3
cycler==0.10.0
decorator==4.3.0
Django==2.0.6
idna==2.6
ipython==6.4.0
ipython-genutils==0.2.0
jedi==0.12.0
JPype1==0.6.3
kiwisolver==1.0.1
matplotlib==2.2.2
numpy==1.14.3
parso==0.2.1
pexpect==4.6.0
pickleshare==0.7.4
prompt-toolkit==1.0.15
ptyprocess==0.5.2
Pygments==2.2.0
pyparsing==2.2.0
python-dateutil==2.7.3
pytimber==2.6.2
pytz==2018.4
requests==2.18.4
scipy==1.1.0
simplegeneric==0.8.1
six==1.11.0
traitlets==4.3.2
urllib3==1.22
wcwidth==0.1.7
